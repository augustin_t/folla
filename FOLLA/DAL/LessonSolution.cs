//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class LessonSolution
    {
        public int LessonSolutionId { get; set; }
        public int UserId { get; set; }
        public int LessonId { get; set; }
        public int CorrectAnswers { get; set; }
    
        public virtual Lesson Lesson { get; set; }
        public virtual User User { get; set; }
    }
}
