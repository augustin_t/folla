﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Linq.Expressions;
using System.Data.Entity;

namespace DAL
{
    public class UnitOfWork : IDisposable
    {
        //database context
        private FollaEntities context = new FollaEntities();

        private bool disposed = false;

        public virtual IQueryable<T> Get<T>(Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "") where T : class
        {
            var dbSet = context.Set<T>();
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query);
            }
            else
            {
                return query;
            }
        }

        public virtual T GetById<T>(object id) where T : class
        {
            var dbSet = context.Set<T>();
            return dbSet.Find(id);
        }

        public T Insert<T>(T a) where T : class
        {
            var dbSet = context.Set<T>();
            if (dbSet == null)
            {
                return null;
            }
            return dbSet.Add(a);
        }

        public virtual void Delete<T>(T entityToDelete) where T : class
        {
            var dbSet = context.Set<T>();
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update<T>(T entityToUpdate) where T : class
        {
            var dbSet = context.Set<T>();
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public void SetLazyLoading(bool lazy)
        {
            this.context.Configuration.LazyLoadingEnabled = lazy;
        }

        public int Save()
        {
            return context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
