﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOLLA.Helpers
{
    public class SessionHelper
    {
        internal class Constants
        {
            public static string UserId = "UserId";
            public static string User = "User";
            public static string Authentificated = "Authentificated";
            public static string CurrentLanguageId = "CurrentLanguageId";
        }

        public static int UserId
        {
            get
            {
                return HttpContext.Current.Session[Constants.UserId] != null ? (int)HttpContext.Current.Session[Constants.UserId] : -1;
            }
            set
            {
                HttpContext.Current.Session[Constants.UserId] = value;
            }
        }

        public static DAL.User User
        {
            get
            {
                return HttpContext.Current.Session[Constants.User] as DAL.User;
            }
            set
            {
                HttpContext.Current.Session[Constants.User] = value;
            }
        }

        public static bool Authentificated
        {
            get
            {
                return UserId != -1;
            }
            private set
            {
                HttpContext.Current.Session[Constants.Authentificated] = value;
            }
        }

        public static int CurrentLanguageId
        {
            get
            {
                return HttpContext.Current.Session[Constants.CurrentLanguageId] != null ? (int)HttpContext.Current.Session[Constants.CurrentLanguageId] : -1;
            }
            set
            {
                HttpContext.Current.Session[Constants.CurrentLanguageId] = value;
            }
        }
    }
}