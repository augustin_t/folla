﻿//Progress Storage - for managing guests users stats

//User class
function User(data) {
    this.UserId = data.UserId;
    this.Score = data.Score;
    this.solutions = {};
    this.achievements = {};

    if (data.solutions) {
        for (var solutionId in data.solutions) {
            this.solutions[solutionId] = new LessonSolution(data.solutions[solutionId]);
        };
    }

    if (data.achievements) {
        for (var achievementId in data.achievements) {
            this.achievements[achievementId] = new Achievement(data.achievements[achievementId]);
        };
    }
}

//Lesson Solution Class
function LessonSolution(data) {
    this.LessonSolutionId = data.LessonSolutionId;
    this.LessonId = data.LessonId;
    this.NumberOfQuestions = data.NumberOfQuestions;
    this.CorrectAnswers = data.CorrectAnswers;
}

//Achievement Class
function Achievement(data) {
    this.AchievementId = data.AchievementId;
    this.Name = data.Name;
    this.Icon = data.Icon;
    this.Score = data.Score;
}

//Returns user solution by solutionId
User.prototype.getSolution = function (solutionId) {
    var solution = this.solutions[solutionId];
    return (solution) ? solution : null;
}

//adds a solution for a lesson
User.prototype.addSolution = function (lessonSolution) {
    if (lessonSolution) {
        this.solutions[lessonSolution.LessonId] = lessonSolution;
    }
}

//Removes a sollution
User.prototype.removeSolution = function (solutionId) {
    delete this.solutions[solutionId];
}

//returns an achievement
User.prototype.getAchievement = function (achievementId) {
    var achievement = this.achievements[achievementId];
    return (achievement) ? achievement : null;
}

//adds an achievement
User.prototype.addAchievement = function (achievement) {
    if (achievement) {
        this.achievements[achievement.AchievementId] = achievement;
    }
}

//removes an achievement
User.prototype.removeAchievement = function (achievementId) {
    delete this.achievements[achievementId];
}

//return  user stats
User.prototype.getStatsData = function () {
    var stats = {};
    stats.score = 0;
    stats.lessonsCompleted = 0;
    stats.lessonsFailed = 0;
    stats.accuracy = 0;

    for (var solutionId in this.solutions) {
        var solution = this.solutions[solutionId];
        if (solution.CorrectAnswers === solution.NumberOfQuestions) {
            stats.lessonsCompleted++;
        } else {
            stats.lessonsFailed++;
        }
        stats.score += solution.CorrectAnswers;
        stats.accuracy += solution.NumberOfQuestions;
    }
    if (stats.accuracy !== 0) {
        stats.accuracy = Math.trunc((stats.score * 100.0) / stats.accuracy);
    }

    return stats;
}

//Progress Storage Class
var ProgressStorage = (function (storage) {

    //saves user to localStorage
    storage.saveUser = function (user) {
        var stringObj = JSON.stringify(user);
        localStorage.setItem(user.UserId, stringObj);
    }

    //gets user from localStorage
    storage.getUser = function (userId) {
        if (userId === undefined) {
            return;
        }
        var result = JSON.parse(localStorage.getItem(userId));
        if (result === null) {
            user = new User({ UserId: userId });
            this.saveUser(user);
        } else {
            user = new User(result);
        }
        return user;
    }

    //saves lesson solution for user to localstorage
    storage.saveLessonSolutionForUser = function (lessonSolution, userId) {
        if (!lessonSolution || !userId)
            return false;
        var user = this.getUser(userId);
        if (user !== null) {
            user.addSolution(lessonSolution);
            this.saveUser(user);
        } else {
            user = new User({ UserId: userId });
            user.addSolution(lessonSolution);
            this.saveUser(user);
        }
        return true;
    }

    //saves achievement for user to localStorage
    storage.saveAchievementForUser = function (achievement, userId) {
        user = this.getUser(userId);
        if (user !== null) {
            user.addSolution(lessonSolution);
            this.saveUser(user);
        }
    }

    return storage;
})(ProgressStorage || {});



