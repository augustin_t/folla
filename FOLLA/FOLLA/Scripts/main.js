﻿(function ($) {
    onDomReady();
    function onDomReady() {
        //add listeners
    }

    var activeWidget;

    function changeActiveWidget(widget) {
        if ($(widget).hasClass('active-widget')) {
            activeWidget = undefined;
        } else {
            if (activeWidget !== undefined) {
                $(activeWidget).toggle();
                $(activeWidget).toggleClass('active-widget');
            }
            activeWidget = widget;
        }
        $(widget).toggleClass('active-widget');
        $(widget).toggle();
    }

  


    //language widget toggle
    $('.iconFlag').on('click', function (event) {
        event.preventDefault();
        var widget = $('.language-widget');
        changeActiveWidget(widget);
    });

    //user widget toggle
    $('.iconUser').on('click', function (event) {
        event.preventDefault();
        var widget = $('.user-widget');
        changeActiveWidget(widget);
    });

    //user widget toggle
    $('.iconDictionary').on('click', function (event) {
        event.preventDefault();
        var widget = $('.dictionary-widget');
        changeActiveWidget(widget);
    });

    $('#user-widget .show-sign-in-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-in-container').first().toggleClass('hidden');
    });

    $('#user-widget .sign-in-container .cancel-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-in-container').first().toggleClass('hidden');
    });

    $('#user-widget .show-sign-up-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-up-container').first().toggleClass('hidden');
    });

    $('#user-widget .sign-up-container .cancel-btn').on('click', function (event) {
        event.preventDefault();
        $('#user-widget .login-btn-container').first().toggleClass('hidden');
        $('#user-widget .sign-up-container').first().toggleClass('hidden');
    });

    //stop propagation of onclick event on widget's area
    $('.widget').on('click', function (event) {
        event.stopPropagation();
    });
})(jQuery);
