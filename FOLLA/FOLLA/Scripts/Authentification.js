﻿//Authentification - used for authentification widged
var authentification = (function () {
    var self = {};

    self.attachEvents = function () {
        $("#btnSignIn").on('click', onBtnSignInClick);
        $("#btnSignUp").on('click', onBtnSignUpClick);
        $("#btnLogout").on('click', onBtnLogoutClick);
    };

    //Sign In button handler
    function onBtnSignInClick(event) {
        event.preventDefault();
        var loginData = getLoginData();
        follaService.login(loginData).done(onLoginDone);
    };


    //Sign Up button handler
    function onBtnSignUpClick(event) {
        event.preventDefault();
        var registerData = getRegisterData();
        follaService.register(registerData).done(onRegisterDone);
    };


    //Logout button handler
    function onBtnLogoutClick(event) {
        event.preventDefault();
        follaService.logout().done(onLogoutDone);
    }

    //returns username and password
    function getLoginData() {
        var loginData = {};
        loginData.Username = $("#input-username-login").val();
        loginData.Password = $("#input-password-login").val();
        return loginData;
    };

    //returns username, email and password
    function getRegisterData() {
        var registerData = {};
        registerData.Username = $("#input-username-register").val();
        registerData.Email = $("#input-email-register").val();
        registerData.Password = $("#input-password-register").val();
        return registerData;
    };

    //Login done handler
    function onLoginDone(data) {
        if (data) {
            window.location.reload(true);
        }
        else {
            Dialog.open("Error","Invalid credentials, please try again");
        }
    };

    //Register done handler
    function onRegisterDone(data) {
        if (data) {
            Dialog.open("Success", "Registered succesfully!", function () { follaService.login(getRegisterData()).done(onLoginDone); });            
        }
        else {
            Dialog.open("Error", "Invalid credentials, please try again");
        }
    };

    //Logout done handler
    function onLogoutDone(data) {
        if (data) {
            window.location.reload(true);
        }
        else {
            Dialog.open("Error", "You are not logged in.");
        }
    };

    return self;
})();

//attach events on ready
$(document).ready(authentification.attachEvents);