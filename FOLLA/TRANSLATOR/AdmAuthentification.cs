﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace TRANSLATOR
{
    class AdmAuthentication
    {
        private static readonly object _flag = new object();
        private static readonly string DatamarketAccessUri = Properties.Settings.Default.DatamarketAccessUri;
        private static DateTime lastRenewTime = DateTime.Now.AddMinutes(-10);
        private static readonly string Method = "POST";
        private static readonly string ContentType = "application/x-www-form-urlencoded";
        private string clientId;
        private string clientSecret;
        private string request;
        private AdmAccessToken token;        

        public AdmAuthentication(string clientId, string clientSecret)
        {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.request = String.Format(Properties.Settings.Default.TokenRequest, HttpUtility.UrlEncode(clientId), HttpUtility.UrlEncode(clientSecret));
        }

        public AdmAccessToken AccessToken
        {
            get
            {
                lock (_flag)
                {
                    if ((DateTime.Now - lastRenewTime).TotalMinutes > 8)
                    {
                        this.token = HttpPost(DatamarketAccessUri, this.request);
                    }
                    return this.token;
                }                
            }
            private set{}
        }

        private void RenewAccessToken()
        {
            AdmAccessToken newAccessToken = HttpPost(DatamarketAccessUri, this.request);
            this.token = newAccessToken;
        }
        
        private AdmAccessToken HttpPost(string DatamarketAccessUri, string requestDetails)
        {
            WebRequest webRequest = WebRequest.Create(DatamarketAccessUri);
            webRequest.ContentType = ContentType;
            webRequest.Method = Method;
            byte[] bytes = Encoding.ASCII.GetBytes(requestDetails);
            webRequest.ContentLength = bytes.Length;
            using (Stream outputStream = webRequest.GetRequestStream())
            {
                outputStream.Write(bytes, 0, bytes.Length);
            }
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(AdmAccessToken));
                //Get deserialized object from JSON stream
                AdmAccessToken token = (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                lastRenewTime = DateTime.Now;
                return token;
            }
        }
    }
}
